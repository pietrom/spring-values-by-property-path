package org.amicofragile.test.spring;

import java.util.Properties;

public class Configuration {
	private final String cat;
	private final String crow;
	private final String dog;

	public Configuration(Properties properties) {
		cat = properties.getProperty("conf.cat");
		crow = properties.getProperty("conf.crow").toUpperCase();
		dog = "{" + properties.getProperty("conf.dog") + "}";
	}

	public String getCat() {
		return cat;
	}

	public String getCrow() {
		return crow;
	}

	public String getDog() {
		return dog;
	}
}
