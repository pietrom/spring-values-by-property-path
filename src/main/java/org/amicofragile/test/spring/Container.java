package org.amicofragile.test.spring;

public class Container {
	private final String value;

	public Container(String value) {
		super();
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
