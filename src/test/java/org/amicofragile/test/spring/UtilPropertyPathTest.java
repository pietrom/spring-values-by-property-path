package org.amicofragile.test.spring;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/util-property-path-context.xml")
public class UtilPropertyPathTest {
	@Resource(name = "catContainer")
	private Container catContainer;

	@Resource(name = "crowContainer")
	private Container crowContainer;
	
	@Resource(name = "dogContainer")
	private Container dogContainer;

	@Test
	public void shouldInjectCatValueInContainer() {
		Assert.assertEquals("mammal", catContainer.getValue());
	}

	@Test
	public void shouldInjectCrowValueInContainer() {
		Assert.assertEquals("BIRD", crowContainer.getValue());
	}
	
	@Test
	public void shouldInjectDogValueInContainer() {
		Assert.assertEquals("{mammal}", dogContainer.getValue());
	}
}
